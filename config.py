"""
Copyright (c) 2010, 2014 dequis
Copyright (c) 2012 Randall Ma
Copyright (c) 2012-2014 Tycho Andersen
Copyright (c) 2012 Craig Barnes
Copyright (c) 2013 horsik
Copyright (c) 2013 Tao Sauvage
Copyright (c) 2022 Rudenco Yehoward

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os
import subprocess as sproc

from libqtile.log_utils import logger
from libqtile import hook, layout, qtile
from libqtile.command import lazy
from libqtile.config import (
    Click,
    Drag,
    Match,
    Rule,
    Screen,
)

# from libqtile.widget import Spacer

from modules.my_keys import my_static_keys, new_layout_keys
from modules.my_vars import my_variables as my
from modules.my_groups import MY_GROUPS, GROUP_SPECIFIC_APPS
from modules.my_layouts import MyLayouts


keys = my_static_keys
groups = MY_GROUPS

for group in groups:
    if group.name != my.scratchpad:
        keys.extend(new_layout_keys(group.name))

layouts = [ l for id, l in MyLayouts() ]

colors = my.colors
screens = [Screen()]

mouse = [
    Drag(
        [my.mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [my.mod],
        "Button3",
        lazy.window.set_size_floating(),
        start=lazy.window.get_size(),
    ),
]

dgroups_key_binder = None
dgroups_app_rules = []

main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "focus"  # or smart
wmname = "LG3D"


floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="confirm"),
        Match(wm_class="dialog"),
        Match(wm_class="download"),
        Match(wm_class="error"),
        Match(wm_class="file_progress"),
        Match(wm_class="notification"),
        Match(wm_class="splash"),
        Match(wm_class="toolbar"),
        Match(wm_class="confirmreset"),
        Match(wm_class="makebranch"),
        Match(wm_class="maketag"),
        Match(wm_class="Arandr"),
        Match(wm_class="feh"),
        Match(wm_class="Galculator"),
        Match(title="branchdialog"),
        Match(title="Open File"),
        Match(title="pinentry"),
        Match(title="Qtile hints"),
        Match(wm_class="ssh-askpass"),
        Match(wm_class="lxpolkit"),
        Match(wm_class="Lxpolkit"),
        Match(wm_class="yad"),
        Match(wm_class="Yad"),
        Match(wm_class="Cairo-dock"),
        Match(wm_class="cairo-dock"),
    ],
    fullscreen_border_width=0,
    border_width=0,
)

# Hooks
@hook.subscribe.client_new
def assign_app_group(client):
    """Assings the opened app to the group, if one specified."""
    wm_class = client.window.get_wm_class()[0]

    for group_name, apps in GROUP_SPECIFIC_APPS.items():
        if wm_class in apps:
            client.togroup(group_name)
            client.group.cmd_toscreen()


@hook.subscribe.startup_once
def start_once():
    """Run the startup script."""
    home = os.path.expanduser("~")
    sproc.call([home + "/.config/qtile/scripts/autostart.sh"])


@hook.subscribe.startup
def start_always():
    """Set the cursor to something sane in X"""
    with sproc.Popen(["xsetroot", "-cursor_name", "left_ptr"]) as proc:
        logger.log(proc)


@hook.subscribe.client_new
def set_floating(window):
    """Set the window to floating"""
    floating_types = ["notification", "toolbar", "splash", "dialog"]

    if (
        window.window.get_wm_transient_for()
        or window.window.get_wm_type() in floating_types
    ):
        window.floating = True


@hook.subscribe.enter_chord
def show_keys(mode):
    """Starts the hints script to display the KeyChord information"""
    home = os.path.expanduser("~")

    with sproc.Popen([home + "/.config/qtile/hints.py", mode], text=True) as proc:
        outs, errs = proc.communicate(timeout=15)

        if outs:
            logger.warning(outs)
        if errs:
            logger.warning(errs)


@hook.subscribe.leave_chord
def close_hints():
    """Kills the hints script"""
    try:
        sproc.run(["killall", "hints.py"], check=True)
    except sproc.CalledProcessError as err:
        logger.warning(err)
