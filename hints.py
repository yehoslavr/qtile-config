#!/usr/bin/env python3
"""A script that generates hints for qtile KeyChords"""
import sys
from hints_py.utils import get_mode
from hints_py.app import create_app
from modules.my_keys import key_templates


def main():
    """The main function that bootstraps the hints app."""
    description: str
    key_bindings: list

    # TODO: use argparse to get the argumetns from comand-line
    mode = sys.argv[1]
    description, key_bindings = get_mode(mode, key_templates)

    app = create_app(key_bindings, description)
    app.mainloop()


if __name__ == "__main__":
    main()
