"""A set of functions to manage the hints.py configuration"""
from pathlib import Path
from typing import NamedTuple
from collections import namedtuple
import subprocess

import toml


Font = namedtuple("Font", ["name", "size", "title_size"])
Colors = namedtuple("Colors", ["bg", "title_bg", "key", "description", "title_fg"])

Padding = namedtuple("Padding", "top bottom")


class Geometry(NamedTuple):
    padding: Padding
    line_height: float
    title_height: float
    nr_cols: int
    ipadx: float
    position: str


class Configuration(NamedTuple):
    colors: Colors
    font: Font
    geometry: Geometry


# Let's improve it !!
RGB = namedtuple("RGB", "red green blue")

def to_rgb(html_color: str) -> RGB:
    """
    Transforms an html style color to an RGB object.

    @param html_color: A color in format #RRGGBB
    """
    hex_val = html_color.removeprefix("#")
    hex_red = hex_val[0:2]
    hex_gre = hex_val[2:4]
    hex_blu = hex_val[4:]
    return RGB(
        int(hex_red, 16),
        int(hex_gre, 16),
        int(hex_blu, 16),
    )

def add_rgb(rgb1: RGB, rgb2: RGB) -> RGB:
    """A functiton that add two RGB values and returns a new RGB object."""
    diff_r, diff_g, diff_b = (
        rgb1.red + rgb2.red,
        rgb1.green + rgb2.green,
        rgb1.blue + rgb2.blue,
    )
    return RGB(
        diff_r if diff_r > 0 else 0,
        diff_g if diff_g > 0 else 0,
        diff_b if diff_b > 0 else 0,
    )

def rgb_to_html(rgb: RGB) -> str:
    """Converts the RGB object to a html color string"""
    return f"#{rgb.red:02X}{rgb.green:02X}{rgb.blue:02X}"

# config
def get_config(config_path: str) -> Configuration:
    """
    Gets the configuration values from the config files and returns a Configuration object.
    """
    path = Path(config_path).expanduser()

    if not path.exists():
        raise ValueError(f"No config file found at: {path}")

    conf: dict = toml.load(path.expanduser())
    use_xrdb: bool = conf["colors"]["use_xrdb"]
    colors: dict = {
        key: value for key, value in conf["colors"].items() if key != "use_xrdb"
    }

    if use_xrdb:

        xrdb = subprocess.check_output(["xrdb", "-query"])
        xrdb_str = xrdb.decode()
        xrdb_colors = [
            tuple(map(lambda x: x.strip(), line.split(":")))
            for line in xrdb_str.split("\n")
        ]

        for key, value in colors.items():
            for xkey, xvalue in xrdb_colors:
                if value in xkey:
                    colors[key] = xvalue
                    break

        bg_color = to_rgb(colors["bg"])
        diff = RGB(-16, -16, -16)

        colors["title_bg"] = rgb_to_html(add_rgb(bg_color, diff))

    return Configuration(
        colors=Colors(**colors),
        font=Font(**conf["font"]),
        geometry=Geometry(
            padding=Padding(**conf["geometry"]["padding"]),
            **{key: val for key, val in conf["geometry"].items() if key != "padding"},
        ),
    )


def get_mode(mode: str, template: list) -> tuple[str, list]:
    """Get the keybindings and description based on called KeyChord mode.

    @param template: A list of key templates from wich the KeyChords are generated"""
    for m in template:
        if m[1] == mode:
            return m[2], m[3]

    return "Unknown KeyChorde", []
