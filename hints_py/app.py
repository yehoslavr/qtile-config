"""
The GPLv3 License (GPLv3)

Copyright (c) 2022 Yehoward Rudenco

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import tkinter as tk
from .utils import get_config, Configuration


# TODO: trebuie de bagat intr-o functie main, sa facem mai butiful :)))


def key_description_lbl(
    master: tk.Tk | tk.Frame, key: str, description: str, conf: Configuration
) -> tk.Frame:
    """
    Generates a key-descrption Widget to give the possibility to use different
    colors for each element.
    """

    frame = tk.Frame(master)
    frame.configure(bg=conf.colors.bg)
    key_lbl = tk.Label(
        frame,
        text=key,
        fg=conf.colors.key,
        font=(conf.font.name, conf.font.size),
        bg=conf.colors.bg,
    )
    description_lbl = tk.Label(
        frame,
        text="- " + description,
        fg=conf.colors.description,
        font=(conf.font.name, conf.font.size),
        bg=conf.colors.bg,
    )

    key_lbl.pack(side=tk.LEFT)
    description_lbl.pack(side=tk.LEFT)

    return frame


def create_app(
    key_bindings: list,
    win_title: str,
    config_file: str = "~/.config/qtile/hints_py/hints_config.toml",
):
    """
    Creates a Tkinter window that displays the keybind with their respective description as given.

    @param key_bindings:
    @param win_title: The title of the window, usualy the description of the KeyChord
    """
    # Definim obiectul in care vom pastra configurarea aplicatitei

    C = get_config(config_file)

    root = tk.Tk()
    root.title("Qtile hints")
    root.configure(bg=C.colors.bg)

    # Generam titlul
    tk.Label(
        root,
        text=win_title,
        font=(C.font.name, C.font.title_size),
        fg=C.colors.title_fg,
        bg=C.colors.title_bg,
    ).pack(fill=tk.X)

    key_frame = tk.Frame(root)
    key_frame.configure(bg=C.colors.bg)
    key_frame.pack()

    cols = C.geometry.nr_cols
    row = 0
    col = 0
    for key in key_bindings:
        msg_lbl = key_description_lbl(
            master=key_frame, key=key[0], description=key[2], conf=C
        )
        msg_lbl.grid(row=row, column=col, sticky=tk.W, ipadx=C.geometry.ipadx)
        col += 1
        if col == cols:
            row += 1
            col = 0

    if col == 0:
        row -= 1

    # TODO: Bagam si ata in config file? Si poate adaugam sa putem seta prin flaguri
    height = (
        C.geometry.title_height
        + (1 + row)
        + C.geometry.line_height
        + C.geometry.padding.bottom
    )

    top = root.winfo_screenheight() - height
    root.geometry(f"{root.winfo_screenwidth()-20}x{height}+10+{top-10}")

    return root
