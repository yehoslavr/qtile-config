"""In this module are defined all the groups."""
from collections import namedtuple
from typing import NamedTuple

from libqtile.config import DropDown, Group, ScratchPad

from .my_vars import my_variables as my


GroupTemplate = namedtuple("GroupTemplate", "name label layout")

group_templates = [
    GroupTemplate("1", "", "bsp"),
    GroupTemplate("2", "", "monadtall"),
    GroupTemplate("3", "切", "bsp"),
    GroupTemplate("4", "", "bsp"),
    GroupTemplate("5", "", "bsp"),
    GroupTemplate("6", "", "bsp"),
    GroupTemplate("7", "", "bsp"),
    GroupTemplate("8", "", "bsp"),
    GroupTemplate("9", "侮", "treetab"),
    GroupTemplate("0", "", "floating"),
]

DropTemplate = namedtuple("DropTemplate", "name cmd kwargs")

base_drowdown_args = {
    "x": 0.05,
    "y": 0.2,
    "width": 0.6,
    "height": 0.55,
    "opacity": 0.9,
    # INFO: Am lăsat tupa po pricolu
    "on_focus_lost_hide": True,
}


class MyDropdowns(NamedTuple):
    """Holds the name and comand to run for all the available DropDowns"""

    telegram: DropTemplate = DropTemplate(
        "telegram",
        "telegram-desktop",
        base_drowdown_args,
    )
    term: DropTemplate = DropTemplate(
        "term",
        my.term,
        # INFO: Dacă o setare din bază nu ne confive o putem modifica așa
        dict(
            base_drowdown_args,
            on_focus_lost_hide=False,
        ),
    )


MY_GROUPS = [
    Group(name=name, layout=layout, label=label)
    for name, label, layout in group_templates
] + [
    ScratchPad(
        my.scratchpad,
        [DropDown(name, cmd, **kwargs) for name, cmd, kwargs in MyDropdowns()],
    )
]

GROUP_SPECIFIC_APPS = {
    "1": [],
    "2": [
        "qutebrowser",
        "Navigator",
        "brave-browser",
    ],
    "3": [],
    "4": [],
    "5": [],
    "6": [],
    "7": [
        "Gimp",
        "Inkscape",
    ],
    "8": [
        "pcmanfm",
        "Nemo",
        "Caja",
        "Nautilus",
        "org.gnome.Nautilus",
        "Pcmanfm",
        "Pcmanfm-qt",
        "pcmanfm",
        "nemo",
        "caja",
        "nautilus",
        "org.gnome.nautilus",
        "pcmanfm",
        "pcmanfm-qt",
    ],
    "9": [
        "Geary",
        "Mail",
        "Thunderbird",
        "evolution",
        "geary",
        "mail",
        "thunderbird",
    ],
    "0": [
        "Pragha",
        "Clementine",
        "Deadbeef",
        "Audacious",
        "pragha",
        "clementine",
        "deadbeef",
        "audacious",
    ],
}
