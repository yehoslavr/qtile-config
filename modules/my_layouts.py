from collections import namedtuple
from typing import NamedTuple
from libqtile import layout

from .my_vars import my_variables as my

layout_theme_base = {
    "margin": 10,
    "border_width": 2,
    "border_focus": my.colors[11][0],
    "border_normal": my.colors[6][0],
}

layout_theme_mt = dict(
    layout_theme_base,
    margin=8,
)


Layout = namedtuple("Layout", "id layout")


class MyLayouts(NamedTuple):
    """
    A colection of layouts i like.
    The id in Layout represents the index number in the list, so if layout are
    added or removed the id should be updated.
    """

    monad_tall: Layout = Layout(0, layout.MonadTall(**layout_theme_mt))
    monad_wide: Layout = Layout(1, layout.MonadWide(**layout_theme_mt))
    matrix: Layout = Layout(2, layout.Matrix(**layout_theme_base))
    bsp: Layout = Layout(3, layout.Bsp(**layout_theme_base))
    floating: Layout = Layout(4, layout.Floating(**layout_theme_base))
    ratio_tile: Layout = Layout(5, layout.RatioTile(**layout_theme_base))
    max: Layout = Layout(6, layout.Max(**layout_theme_base))
    columns: Layout = Layout(7, layout.Columns(**layout_theme_base))
    stack: Layout = Layout(8, layout.Stack(**layout_theme_base))
    tile: Layout = Layout(9, layout.Tile(**layout_theme_base))
    treetab: Layout = Layout(
        10,
        layout.TreeTab(
            sections=["FIRST", "SECOND"],
            bg_color=my.colors[14][0],
            active_bg=my.colors[23][0],
            inactive_bg=my.colors[22][0],
            padding_y=5,
            place_right=True,
            section_top=10,
            panel_width=180,
        ),
    )
    vertical_tile: Layout = Layout(11, layout.VerticalTile(**layout_theme_base))
    zoomy: Layout = Layout(12, layout.Zoomy(**layout_theme_base))
    monad_three_col: Layout = Layout(13, layout.MonadThreeCol(**layout_theme_base))
